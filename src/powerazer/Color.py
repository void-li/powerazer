#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import re

# ----------

class ColorException(Exception):
  pass

# ----------

class Color:
  REGEX = re.compile('#(?P<r>[a-fA-F0-9]{2})(?P<g>[a-fA-F0-9]{2})(?P<b>[a-fA-F0-9]{2})')

  def __init__(self, r, g, b):
    self.r = self.__clamp(r, 000, 255)
    self.g = self.__clamp(g, 000, 255)
    self.b = self.__clamp(b, 000, 255)

  @staticmethod
  def construct(string):
    match = Color.REGEX.match(string)

    if not match:
      raise ColorException(
        'Invalid color string "{}"'.format(string)
      )

    r = int(match.group('r'), 16)
    g = int(match.group('g'), 16)
    b = int(match.group('b'), 16)

    return Color(r, g, b)

  def amalgamate(self, some, factor):
    factor = self.__clamp(factor, 0.00, 1.00)

    r = factor * self.r + (1.00 - factor) * some.r
    g = factor * self.g + (1.00 - factor) * some.g
    b = factor * self.b + (1.00 - factor) * some.b

    return Color(r, g, b)

  @staticmethod
  def __clamp(value, lower, upper):
    value = max(value, lower)
    value = min(value, upper)

    return value

  def __str__(self):
    return '{}(r={},g={},b={})'.format(
      self.__class__.__name__, self.r, self.g, self.b
    )
