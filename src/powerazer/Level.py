#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import re

# ----------

class LevelException(Exception):
  pass

# ----------

class Level:
  REGEX = re.compile('(?P<l>[0-9]+)')

  def __init__(self, l):
    self.l = self.__clamp(l, 000, 100)

  @staticmethod
  def construct(string):
    match = Level.REGEX.match(string)

    if not match:
      raise LevelException(
        'Invalid level string "{}"'.format(string)
      )

    l = int(match.group('l'), 10)

    return Level(l)

  def amalgamate(self, some, factor):
    factor = self.__clamp(factor, 0.00, 1.00)

    l = factor * self.l + (1.00 - factor) * some.l

    return Level(l)

  @staticmethod
  def __clamp(value, lower, upper):
    value = max(value, lower)
    value = min(value, upper)

    return value

  def __str__(self):
    return '{}(l={})'.format(
      self.__class__.__name__, self.l
    )
