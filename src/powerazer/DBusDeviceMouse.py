#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

from . import DBusDevice

# ----------

class DBusDeviceMouse(DBusDevice.DBusDevice):
  def __init__(self, session_bus, serial):
    super().__init__(session_bus, serial)

    self.__mouse_iface = None
    self.__power_iface = None

  async def _setup(self):
    self.__mouse_iface = self._proxy_object.get_interface(
      'razer.device.misc'
    )

    self.__power_iface = self._proxy_object.get_interface(
      'razer.device.power'
    )

  async def _close(self):
    if self.__power_iface:
      self.__power_iface = None

  async def connected(self):
    return await self.__mouse_iface.call_get_firmware() != 'v0.0'

  async def battery_level(self):
    return await self.__power_iface.call_get_battery()

  async def is_charging(self):
    return await self.__power_iface.call_is_charging()
