#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

from . import Device

# ----------

class Mouse(Device.Device):
  def __init__(self, event_loop, config, device):
    super().__init__(event_loop)

    self.__config = config
    self.__device = device

  @property
  def serial(self):
    return self.__device.serial

  async def connected(self):
    return await self.__device.connected()

  async def battery_level(self):
    return await self.__device.battery_level()

  async def is_charging(self):
    return await self.__device.is_charging()
