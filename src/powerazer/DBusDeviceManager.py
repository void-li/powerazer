#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import dbus_next
import dbus_next.aio

from . import DBusDeviceMouse
from . import DBusDeviceMouseDock
from . import Object

# ----------

class DBusDeviceManager(Object.Object):
  DBUS_NAME = 'org.razer'
  DBUS_PATH = '/org/razer'

  def __init__(self):
    super().__init__()

    self.device_created_callback = None
    self.device_removed_callback = None

    self.__session_bus = None
    self.__proxy_object = None
    self.__iface_object = None

    self.__devices = {}

  async def setup(self):
    self._logger.debug('%s.setup()', self)

    self.__session_bus = await dbus_next.aio.MessageBus().connect()

    self.__proxy_object = self.__session_bus.get_proxy_object(
      self.DBUS_NAME, self.DBUS_PATH, await self.__session_bus.introspect(self.DBUS_NAME, self.DBUS_PATH)
    )

    self.__iface_object = self.__proxy_object.get_interface(
      'razer.devices'
    )

    # noinspection PyUnresolvedReferences
    self.__iface_object.on_device_added  (self.__on_device_created)

    # noinspection PyUnresolvedReferences
    self.__iface_object.on_device_removed(self.__on_device_removed)

  async def close(self):
    self._logger.debug('%s.close()', self)

    self.__devices = {}

    if self.__iface_object:
      self.__iface_object = None

    if self.__proxy_object:
      self.__proxy_object = None

    if self.__session_bus:
      self.__session_bus.disconnect()

  def __on_device_created(self):
    self._logger.debug('Device created.')

    if self.device_created_callback:
      self.device_created_callback()

  def __on_device_removed(self):
    self._logger.debug('Device removed.')

    if self.device_removed_callback:
      self.device_removed_callback()

  async def devices(self):
    device_serials = await self.__iface_object.call_get_devices()

    for device_serial in self.__devices:
      if device_serial not in device_serials:
        self._logger.debug('Remove device "%s".', device_serial)

        await self.__devices[device_serial].close()
        del self.__devices[device_serial]

    for device_serial in device_serials:
      self._logger.debug('Found device "%s".', device_serial)

      if device_serial in self.__devices:
        continue

      device = None

      if not device:
        device_type_message = await self.__session_bus.call(
          dbus_next.Message(
            self.DBUS_NAME,
            self.DBUS_PATH + '/device/' + device_serial,
            'razer.device.misc',
            'getDeviceType'
          )
        )

        if device_type_message.body and device_type_message.body[0] == 'mouse':
          self._logger.debug('Create device "%s" of type mouse.', device_serial)

          device = DBusDeviceMouse    .DBusDeviceMouse    (self.__session_bus, device_serial)

      if not device:
        device_name_message = await self.__session_bus.call(
          dbus_next.Message(
            self.DBUS_NAME,
            self.DBUS_PATH + '/device/' + device_serial,
            'razer.device.misc',
            'getDeviceName'
          )
        )

        if device_name_message.body and 'Mouse Dock' in device_name_message.body[0]:
          self._logger.debug('Create device "%s" of type mouse dock.', device_serial)

          device = DBusDeviceMouseDock.DBusDeviceMouseDock(self.__session_bus, device_serial)

      if not device:
        continue

      self.__devices[device_serial] = device
      await self.__devices[device_serial].setup()

    return self.__devices.values()
