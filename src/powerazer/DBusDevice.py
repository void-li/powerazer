#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

from . import Object

# ----------

class DBusDevice(Object.Object):
  DBUS_NAME = 'org.razer'
  DBUS_PATH = '/org/razer/device/{}'

  def __init__(self, session_bus, serial):
    super().__init__()

    self._session_bus = session_bus
    self._proxy_object = None

    self.serial = serial

  async def setup(self):
    self._logger.debug('%s.setup()', self)

    dbus_name = self.DBUS_NAME
    dbus_path = self.DBUS_PATH.format(self.serial)

    self._proxy_object = self._session_bus.get_proxy_object(
      dbus_name, dbus_path, await self._session_bus.introspect(dbus_name, dbus_path)
    )

    await self._setup()

  async def _setup(self):
    pass

  async def close(self):
    self._logger.debug('%s.close()', self)

    await self._close()

    if self._proxy_object:
      self._proxy_object = None

  async def _close(self):
    pass

  def __str__(self):
    return '{}(serial="{}")'.format(
      self.__class__.__name__, self.serial
    )
