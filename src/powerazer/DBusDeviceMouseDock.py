#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

from . import DBusDevice

# ----------

class DBusDeviceMouseDock(DBusDevice.DBusDevice):
  def __init__(self, session_bus, serial):
    super().__init__(session_bus, serial)

    self.__color_iface = None
    self.__gamma_iface = None

  async def _setup(self):
    self.__color_iface = self._proxy_object.get_interface(
      'razer.device.lighting.chroma'
    )

    self.__gamma_iface = self._proxy_object.get_interface(
      'razer.device.lighting.brightness'
    )

  async def _close(self):
    if self.__gamma_iface:
      self.__gamma_iface = None

    if self.__color_iface:
      self.__color_iface = None

  async def set_color(self, color):
    self._logger.debug('%s.set_color(color=%s)', self, color)

    await self.__color_iface.call_set_static(
      int(color.r), int(color.g), int(color.b)
    )

  async def set_gamma(self, gamma):
    self._logger.debug('%s.set_gamma(gamma=%s)', self, gamma)

    await self.__gamma_iface.call_set_brightness(
      int(gamma.g)
    )
