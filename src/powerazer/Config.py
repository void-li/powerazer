#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

from . import Color
from . import Gamma
from . import Level

# ----------

class ConfigException(Exception):
  pass

# ----------

class Config:
  CONFIG = 'config'

  def __init__(self, parser):
    self.__parser = parser

  def devices_interval(self):
    sections = self.CONFIG,
    return float(
      self.__config(sections, 'devices_interval', 60.00)
    )

  def updates_interval(self):
    sections = self.CONFIG,
    return float(
      self.__config(sections, 'updates_interval', 10.00)
    )

  def charged_color(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'charged_color', '#0000FF')
    )

  def charged_gamma(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'charged_gamma', '75')
    )

  def charged_level(self, serial):
    sections = serial, self.CONFIG
    return Level.Level.construct(
      self.__config(sections, 'charged_level', '100')
    )

  def charging_color_max(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'charging_color_max', '#00FF00')
    )

  def charging_color_min(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'charging_color_min', '#00FF00')
    )

  def charging_gamma_max(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'charging_gamma_max', '75')
    )

  def charging_gamma_min(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'charging_gamma_min', '25')
    )

  def charging_intervals(self, serial):
    sections = serial, self.CONFIG
    return float(
      self.__config(sections, 'charging_intervals', 5.00)
    )

  def draining_color_max(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'draining_color_max', '#00FF00')
    )

  def draining_color_min(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'draining_color_min', '#00FF00')
    )

  def draining_gamma_max(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'draining_gamma_max', '75')
    )

  def draining_gamma_min(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'draining_gamma_min', '25')
    )

  def draining_intervals(self, serial):
    sections = serial, self.CONFIG
    return float(
      self.__config(sections, 'draining_intervals', 5.00)
    )

  def drained_color(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'drained_color', '#FF0000')
    )

  def drained_gamma(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'drained_gamma', '25')
    )

  def drained_level(self, serial):
    sections = serial, self.CONFIG
    return Level.Level.construct(
      self.__config(sections, 'drained_level', '000')
    )

  def missing_color(self, serial):
    sections = serial, self.CONFIG
    return Color.Color.construct(
      self.__config(sections, 'missing_color', '#000000')
    )

  def missing_gamma(self, serial):
    sections = serial, self.CONFIG
    return Gamma.Gamma.construct(
      self.__config(sections, 'missing_gamma', '00')
    )

  def mouse_serial(self, serial):
    sections = serial,
    return (
      self.__config(sections, 'mouse')
    )

  def __config(self, sections, option, default=None):
    for section in sections:
      if self.__parser.has_option(section, option):
        return self.__parser.get(section, option)

    return default
