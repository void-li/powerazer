#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import appdirs
import argparse
import asyncio
import configparser
import contextlib
import dbus.exceptions
import logging
import signal

from . import Config
from . import DBusDeviceManager
from . import DBusDeviceMouse
from . import DBusDeviceMouseDock
from . import Mouse
from . import MouseDock
from . import Object

# ----------

class PowerazerException(Exception):
  pass

# ----------

class Powerazer(Object.Object):
  def __init__(self, event_loop, config):
    super().__init__()

    self.__event_loop = event_loop
    self.__config     = config

    self.__device_manager = DBusDeviceManager.DBusDeviceManager()
    self.__device_manager.device_created_callback = self.__on_device_created
    self.__device_manager.device_removed_callback = self.__on_device_removed

    self.__mouse_roster = {}
    self.__mdock_roster = {}

    self.__devices_task = None
    self.__devices_task_event = asyncio.Event()

    self.__updates_task = None
    self.__updates_task_event = asyncio.Event()

  async def setup(self):
    self._logger.debug('%s.setup()', self)

    await self.__device_manager.setup()

    self.__devices_task = self.__event_loop.create_task(
      self.__devices()
    )

    self.__updates_task = self.__event_loop.create_task(
      self.__updates()
    )

  async def close(self):
    self._logger.debug('%s.close()', self)

    if self.__updates_task:
      self.__updates_task.cancel()
      await self.__updates_task
      self.__updates_task = None

    if self.__devices_task:
      self.__devices_task.cancel()
      await self.__devices_task
      self.__devices_task = None

    await self.__device_manager.close()

  async def __devices(self):
    try:
      self._logger.debug('%s.__devices()', self)

      devices_interval = self.__config.devices_interval()
      self._logger.debug('devices_interval=%s', devices_interval)

      try:
        while True:
          try:
            self.__devices_task_event.clear()

            something_changed = False

            mouse_serials = set()
            mdock_serials = set()

            for device in await self.__device_manager.devices():
              self._logger.debug('Found device "%s".', device.serial)

              if isinstance(device, DBusDeviceMouse.DBusDeviceMouse):
                mouse_serial = str(device.serial)
                mouse_serials.add(mouse_serial)

                if mouse_serial not in self.__mouse_roster:
                  await self.__create_mouse(mouse_serial, device)
                  something_changed = True

              if isinstance(device, DBusDeviceMouseDock.DBusDeviceMouseDock):
                mdock_serial = str(device.serial)
                mdock_serials.add(mdock_serial)

                if mdock_serial not in self.__mdock_roster:
                  await self.__create_mdock(mdock_serial, device)
                  something_changed = True

            for mouse_serial in set(self.__mouse_roster):
              if mouse_serial not in mouse_serials:
                await self.__remove_mouse(mouse_serial)
                something_changed = True

            for mdock_serial in set(self.__mdock_roster):
              if mdock_serial not in mdock_serials:
                await self.__remove_mdock(mdock_serial)
                something_changed = True

            if something_changed:
              self.__updates_task_event.set()

            with contextlib.suppress(asyncio.TimeoutError):
              await asyncio.wait_for(self.__devices_task_event.wait(), devices_interval)

          except asyncio.CancelledError as e:
            raise e

          except              Exception as e:
            self._logger.error(e, exc_info = True)
            await asyncio.sleep(1)

      finally:
        for mouse_serial in set(self.__mouse_roster):
          await self.__remove_mouse(mouse_serial)

        for mdock_serial in set(self.__mdock_roster):
          await self.__remove_mdock(mdock_serial)

    except asyncio.CancelledError:
      pass

    except              Exception as e:
      self._logger.error(e, exc_info = True)

    finally:
      self._logger.debug('%s.__devices() done', self)

  async def __create_mouse(self, mouse_serial, device):
    if mouse_serial in self.__mouse_roster:
      raise PowerazerException(
        'Device "{}" of type mouse exists already.'.format(mouse_serial)
      )

    self._logger.debug('Create device "%s" of type mouse.', mouse_serial)

    self.__mouse_roster[mouse_serial] = Mouse    .Mouse    (self.__event_loop, self.__config, device)

  async def __remove_mouse(self, mouse_serial):
    if mouse_serial in self.__mouse_roster:
      self._logger.debug('Remove device "%s" of type mouse.', mouse_serial)

      await self.__mouse_roster[mouse_serial].close()
      del self.__mouse_roster[mouse_serial]

  async def __create_mdock(self, mdock_serial, device):
    if mdock_serial in self.__mdock_roster:
      raise PowerazerException(
        'Device "{}" of type mouse dock exists already.'.format(mdock_serial)
      )

    self._logger.debug('Create device "%s" of type mouse dock.', mdock_serial)

    self.__mdock_roster[mdock_serial] = MouseDock.MouseDock(self.__event_loop, self.__config, device)

  async def __remove_mdock(self, mdock_serial):
    if mdock_serial in self.__mdock_roster:
      self._logger.debug('Remove device "%s" of type mouse dock.', mdock_serial)

      await self.__mdock_roster[mdock_serial].close()
      del self.__mdock_roster[mdock_serial]

  def __on_device_created(self):
    self.__devices_task_event.set()

  def __on_device_removed(self):
    self.__devices_task_event.set()

  async def __updates(self):
    try:
      self._logger.debug('%s.__updates()', self)

      updates_interval = self.__config.updates_interval()
      self._logger.debug('updates_interval=%s', updates_interval)

      while True:
        try:
          self.__updates_task_event.clear()

          failed_mouse_serials = set()
          treated_mouse_serials = set()

          failed_mdock_serials = set()
          treated_mdock_serials = set()

          for mdock_serial, mdock in self.__mdock_roster.items():
            try:
              mouse_serial, mouse = None, None

              serial = self.__config.mouse_serial(mdock_serial)

              if not (mouse_serial and mouse):
                mouse_serial = serial

                if mouse_serial and mouse_serial in self.__mouse_roster:
                  self._logger.debug('Match mouse "%s" to mouse dock "%s" via serial.', mouse_serial, mdock_serial)

                  mouse = self.__mouse_roster[mouse_serial]

              if not (mouse_serial and mouse):
                mouse_serial = ([
                  mouse_serial for mouse_serial in self.__mouse_roster if mouse_serial not in treated_mouse_serials
                ] + [ None ])[0]

                if mouse_serial and mouse_serial in self.__mouse_roster:
                  self._logger.debug('Match mouse "%s" to mouse dock "%s" via search.', mouse_serial, mdock_serial)

                  mouse = self.__mouse_roster[mouse_serial]

              if not (mouse_serial and mouse):
                self._logger.debug('No mouse matched for mouse dock "%s".', mdock_serial)

                continue

              treated_mouse_serials.add(mouse_serial)

              try:
                connected = await mouse.connected()
                self._logger.debug('Mouse "%s" connected=%s.', mouse, connected)

                is_charging = await mouse.is_charging()
                self._logger.debug('Mouse "%s" is_charging=%s.', mouse, is_charging)

                battery_level = await mouse.battery_level()
                self._logger.debug('Mouse "%s" battery_level=%s.', mouse, battery_level)

              except dbus.exceptions.DBusException as e:
                self._logger.warn(e)

                failed_mouse_serials.add(mouse_serial)
                continue

              try:
                if connected:
                  if is_charging:
                    mdock.show_charging(battery_level)

                  else:
                    mdock.show_draining(battery_level)

                else:
                  mdock.show_missing()

              except dbus.exceptions.DBusException as e:
                self._logger.warn(e)

                failed_mdock_serials.add(mdock_serial)
                continue

              treated_mdock_serials.add(mdock_serial)

            except asyncio.CancelledError as e:
              raise e

            except              Exception as e:
              self._logger.error(e, exc_info = True)
              continue

          for mouse_serial in failed_mouse_serials:
            await self.__remove_mouse(mouse_serial)

          for mdock_serial in failed_mdock_serials:
            await self.__remove_mdock(mdock_serial)

          with contextlib.suppress(asyncio.TimeoutError):
            await asyncio.wait_for(self.__updates_task_event.wait(), updates_interval)

        except asyncio.CancelledError as e:
          raise e

        except              Exception as e:
          self._logger.error(e, exc_info = True)
          await asyncio.sleep(1)

    except asyncio.CancelledError:
      pass

    except              Exception as e:
      self._logger.error(e, exc_info = True)

    finally:
      self._logger.debug('%s.__updates() done', self)

# ----------

class Main(Object.Object):
  def __init__(self):
    super().__init__()

    self.__event_loop       = None
    self.__event_loop_alive = None

  def main(self, exec_config=None):
    self._logger.debug('%s.main()', self)

    try:
      self.__event_loop       = asyncio.get_event_loop()
      self.__event_loop_alive = True

    except RuntimeError as e:
      self._logger.debug(e)

      self.__event_loop       = asyncio.new_event_loop()
      self.__event_loop_alive = True

    try:
      for sig in signal.SIGINT, signal.SIGHUP, signal.SIGTERM:
        self.__event_loop.add_signal_handler(sig, self.exit)

      appname = Powerazer.__name__.lower()

      site_config = (list(filter(None,
        [ p.replace('/xdg', '') for p in appdirs.site_config_dir(appname, multipath = True).split(':') if 'etc' in p ]
      )) + [ '' ])[0]; site_config = '{}/{}.ini'.format(site_config, appname) if site_config else ''
      self._logger.info('Site config: "%s".', site_config)

      user_config = (list(filter(None,
        [                                appdirs.user_config_dir(appname)                                            ]
      )) + [ '' ])[0]; user_config = '{}/{}.ini'.format(user_config, appname) if user_config else ''
      self._logger.info('User config: "%s".', user_config)

      exec_config = (list(filter(None,
        [ exec_config ]
      )) + [ '' ])[0]
      self._logger.info('Exec config: "%s".', exec_config)

      parser = configparser.ConfigParser()
      parser.read([
        site_config, user_config, exec_config
      ])

      config = Config.Config(parser)

      powerazer = Powerazer(self.__event_loop, config)

      try:
        self.__event_loop.run_until_complete(powerazer.setup())

        self.__event_loop.run_forever()

      finally:
        self.__event_loop.run_until_complete(powerazer.close())

      for task in asyncio.all_tasks(self.__event_loop):
        task.cancel()

        try:
          self.__event_loop.run_until_complete(task)

        except asyncio.CancelledError:
          pass

        except              Exception as e:
          self._logger.error(e, exc_info = True)

    finally:
      self.__event_loop.close()
      self.__event_loop_alive = False

  def exit(self):
    if self.__event_loop and self.__event_loop_alive:
      self.__event_loop.stop()
      self.__event_loop_alive = False

# ----------

def main():
  def loggingBasicConfig(level):
    logging.basicConfig(level = level, force = True,
      format = '[%(asctime)s %(levelname).1s %(lineno)4d] %(message)s'
    )

  loggingBasicConfig(logging.ERROR)

  parser = argparse.ArgumentParser()
  parser.add_help = True

  parser.add_argument(
    '--log-level',
    choices = [ 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL' ],
    nargs =  1
  )

  parser.add_argument(
    'config_file', metavar = 'config-file',
    nargs = '?'
  )

  args = parser.parse_args()

  if args.log_level and args.log_level[0]:
    loggingBasicConfig(args.log_level[0])

  main = Main()
  main.main(args.config_file)

if __name__ == '__main__':
  main()
