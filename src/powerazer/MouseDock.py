#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import asyncio

from . import Color
from . import Gamma
from . import Device

# ----------

class MouseDock(Device.Device):
  def __init__(self, event_loop, config, device):
    super().__init__(event_loop)

    self.__config = config
    self.__device = device

    self.__battery_level = 0

    self.__current_color = None
    self.__current_gamma = None

    self.__show_charging_task = None
    self.__show_draining_task = None
    self.__show_missing_task  = None

  async def _close(self):
    if self.__show_draining_task:
      self.__show_draining_task.cancel()
      await self.__show_draining_task
      self.__show_draining_task = None

    if self.__show_charging_task:
      self.__show_charging_task.cancel()
      await self.__show_charging_task
      self.__show_charging_task = None

    if self.__show_missing_task:
      self.__show_missing_task.cancel()
      await self.__show_missing_task
      self.__show_missing_task = None

    await self.__colorize(Color.Color(0, 0, 0), Gamma.Gamma(0))

  @property
  def serial(self):
    return self.__device.serial

  def show_charging(self, battery_level):
    self._logger.debug('%s.show_charging(battery_level=%s)', self, battery_level)

    self.__battery_level = battery_level

    if self.__show_draining_task:
      self.__show_draining_task.cancel()
      self.__show_draining_task = None

    if self.__show_missing_task:
      self.__show_missing_task.cancel()
      self.__show_missing_task = None

    if not self.__show_charging_task:
      self.__show_charging_task = self._event_loop.create_task(
        self.__show_charging()
      )

  async def __show_charging(self):
    try:
      self._logger.debug('%s.__show_charging()', self)

      charged_color = self.__config.charged_color(self.serial)
      charged_gamma = self.__config.charged_gamma(self.serial)
      charged_level = self.__config.charged_level(self.serial)

      charging_color_max = self.__config.charging_color_max(self.serial)
      charging_color_min = self.__config.charging_color_min(self.serial)

      charging_gamma_max = self.__config.charging_gamma_max(self.serial)
      charging_gamma_min = self.__config.charging_gamma_min(self.serial)

      charging_intervals = self.__config.charging_intervals(self.serial)

      while True:
        try:
          if self.__battery_level >= charged_level.l:
            self._logger.debug('Show mouse dock "%s" as charged.', self)

            color = charged_color
            gamma = charged_gamma

            await self.__colorize(color, gamma)
            await asyncio.sleep(charging_intervals)

          else:
            self._logger.debug('Show mouse dock "%s" as charging.', self)

            color = charging_color_max.amalgamate(charging_color_min, self.__battery_level)
            gamma = charging_gamma_max.amalgamate(charging_gamma_min, self.__battery_level)

            await self.__colorize(color, gamma)
            await asyncio.sleep(charging_intervals)

            color = Color.Color(0, 0, 0)

            await self.__colorize(color, gamma)
            await asyncio.sleep(charging_intervals)

        except asyncio.CancelledError as e:
          raise e

        except              Exception as e:
          self._logger.error(e, exc_info = True)
          await asyncio.sleep(1)

    except asyncio.CancelledError:
      pass

    except              Exception as e:
      self._logger.error(e, exc_info = True)

    finally:
      self._logger.debug('%s.__show_charging() done', self)

  def show_draining(self, battery_level):
    self._logger.debug('%s.show_draining(battery_level=%s)', self, battery_level)

    self.__battery_level = battery_level

    if self.__show_charging_task:
      self.__show_charging_task.cancel()
      self.__show_charging_task = None

    if self.__show_missing_task:
      self.__show_missing_task.cancel()
      self.__show_missing_task = None

    if not self.__show_draining_task:
      self.__show_draining_task = self._event_loop.create_task(
        self.__show_draining()
      )

  async def __show_draining(self):
    try:
      self._logger.debug('%s.__show_draining()', self)

      drained_color = self.__config.drained_color(self.serial)
      drained_gamma = self.__config.drained_gamma(self.serial)
      drained_level = self.__config.drained_level(self.serial)

      draining_color_max = self.__config.draining_color_max(self.serial)
      draining_color_min = self.__config.draining_color_min(self.serial)

      draining_gamma_max = self.__config.draining_gamma_max(self.serial)
      draining_gamma_min = self.__config.draining_gamma_min(self.serial)

      draining_intervals = self.__config.draining_intervals(self.serial)

      while True:
        try:
          if self.__battery_level <= drained_level.l:
            self._logger.debug('Show mouse dock "%s" as drained.', self)

            color = drained_color
            gamma = drained_gamma

            await self.__colorize(color, gamma)
            await asyncio.sleep(draining_intervals)

          else:
            self._logger.debug('Show mouse dock "%s" as draining.', self)

            color = draining_color_max.amalgamate(draining_color_min, self.__battery_level)
            gamma = draining_gamma_max.amalgamate(draining_gamma_min, self.__battery_level)

            await self.__colorize(color, gamma)
            await asyncio.sleep(draining_intervals)

        except asyncio.CancelledError as e:
          raise e

        except              Exception as e:
          self._logger.error(e, exc_info = True)
          await asyncio.sleep(1)

    except asyncio.CancelledError:
      pass

    except              Exception as e:
      self._logger.error(e, exc_info = True)

    finally:
      self._logger.debug('%s.__show_draining() done', self)

  def show_missing(self):
    self._logger.debug('%s.show_missing()', self)

    if self.__show_charging_task:
      self.__show_charging_task.cancel()
      self.__show_charging_task = None

    if self.__show_draining_task:
      self.__show_draining_task.cancel()
      self.__show_draining_task = None

    if not self.__show_missing_task:
      self.__show_missing_task = self._event_loop.create_task(
        self.__show_missing()
      )

  async def __show_missing(self):
    try:
      self._logger.debug('%s.__show_missing()', self)

      missing_color = self.__config.missing_color(self.serial)
      missing_gamma = self.__config.missing_gamma(self.serial)

      color = missing_color
      gamma = missing_gamma

      await self.__colorize(color, gamma)

    except asyncio.CancelledError:
      pass

    except              Exception as e:
      self._logger.error(e, exc_info = True)

    finally:
      self._logger.debug('%s.__show_missing() done', self)

  async def __colorize(self, color, gamma):
    self._logger.debug('%s.__colorize(color=%s,gamma=%s)', self, color, gamma)

    if isinstance(color, Color.Color):
      if self.__current_color != color:
        self.__current_color = None

        await self.__device.set_color(color)
        self.__current_color = color

    if isinstance(gamma, Gamma.Gamma):
      if self.__current_gamma != gamma:
        self.__current_gamma = None

        await self.__device.set_gamma(gamma)
        self.__current_gamma = gamma
