#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import re

# ----------

class GammaException(Exception):
  pass

# ----------

class Gamma:
  REGEX = re.compile('(?P<g>[0-9]+)')

  def __init__(self, g):
    self.g = self.__clamp(g, 000, 100)

  @staticmethod
  def construct(string):
    match = Gamma.REGEX.match(string)

    if not match:
      raise GammaException(
        'Invalid gamma string "{}"'.format(string)
      )

    g = int(match.group('g'), 10)

    return Gamma(g)

  def amalgamate(self, some, factor):
    factor = self.__clamp(factor, 0.00, 1.00)

    g = factor * self.g + (1.00 - factor) * some.g

    return Gamma(g)

  @staticmethod
  def __clamp(value, lower, upper):
    value = max(value, lower)
    value = min(value, upper)

    return value

  def __str__(self):
    return '{}(g={})'.format(
      self.__class__.__name__, self.g
    )
