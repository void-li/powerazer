#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import inspect
import logging

# ----------

class Object:
  def __init__(self):
    self._logger = self.create_logger(self.__class__)

  @staticmethod
  def create_logger(class_):
    if inspect.isclass(class_):
      return logging.getLogger(class_.__qualname__)

    return logging.getLogger(class_             )

  def __str__(self):
    return '{}'.format(
      self.__class__.__name__
    )
