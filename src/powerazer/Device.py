#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import asyncio

from . import Object

# ----------

class Device(Object.Object):
  def __init__(self, event_loop):
    super().__init__()

    self._event_loop = event_loop
    self.__lock = asyncio.Lock()

  @property
  def serial(self):
      return None

  async def setup(self):
    self._logger.debug('%s.setup()', self)

    await self._setup()

  async def _setup(self):
    pass

  async def close(self):
    self._logger.debug('%s.close()', self)

    await self._close()

  async def _close(self):
    pass

  async def __aenter__(self):
    await self.__lock.__aenter__()

  async def __aexit__(self, exc_type, exc_val, exc_tb):
    await self.__lock.__aexit__(exc_type, exc_val, exc_tb)

  def __str__(self):
    return '{}(serial={})'.format(
      self.__class__.__name__, self.serial
    )
