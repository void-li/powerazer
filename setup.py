#
# Copyright © 2022 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

import setuptools

# ----------

if __name__ == '__main__':
  setuptools.setup(
    name='Powerazer',
    version='v0.0.0',
    description='A simple tool for Linux to show the battery level of your Razer wireless mouse with it\'s mouse dock.',
    author='Luca Lovisa',
    author_email='opensource@void.li',
    license='WTFPL',
    url='https://gitlab.com/void-li/powerazer',
    install_requires=[
      'appdirs', 'argparse', 'dbus-next'
    ],
    packages=[
      'powerazer'
    ],
    package_dir={
      '': 'src'
    },
    entry_points={
      'console_scripts': [
        'powerazer=powerazer.Powerazer:main'
      ],
    }
  )
